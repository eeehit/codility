package com.company.Lesson1;

/**
 * Created by trinity on 2017-10-21.
 */
//https://codility.com/demo/results/trainingJU6QNU-NZ9/
//100%
class BinaryGap {
    public static int solution(int N) {
        char[] binN = Integer.toBinaryString(N).toCharArray();
        int boundary = -1;
        int minZero = 0;

        for(int i=0 ; i<binN.length ; i++) {
            if (binN[i] == '1'){ // 1을 만나면
                if (boundary != -1) {
                    int currentBoundary = i;
                    int beforeBoundary = boundary;
                    int currentResult = currentBoundary - beforeBoundary - 1;
                    if ( minZero < currentResult) {
                        minZero = currentResult;
                    }
                }
                boundary = i;
            }
        }

        return minZero;
    }
}