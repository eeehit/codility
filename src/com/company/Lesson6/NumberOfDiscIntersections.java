package com.company.Lesson6;

import java.util.*;

//https://codility.com/demo/results/trainingKXE8RW-TDS/
//56%
//https://codility.com/demo/results/trainingGV2JCH-SAF/
//62%
//https://codility.com/demo/results/training8ZRTSS-DPY/
//56%
//https://codility.com/demo/results/trainingXJUFHH-EZR/
//62%
//https://codility.com/demo/results/training836F55-9BN/
//75%
//https://codility.com/demo/results/training5VC3JT-QSM/
//https://codility.com/demo/results/trainingXKPKJZ-H7J/
//87%
public class NumberOfDiscIntersections {
    public static int solution(int[] A) {
        int output = 0;
        int N = A.length;
        class Circle {
            long start;
            long end;
        }
        Circle [] circles = new Circle[N];

        //Circle 생성
        for(int i=0 ; i<N ; i++) {
            Circle circle = new Circle();
            circle.start = i-A[i];
            circle.end = (long)i+A[i];
            circles[i] = circle;
        }

        //Circle 정렬
        Arrays.sort(circles, new Comparator<Circle>() {
            @Override
            public int compare(Circle t0, Circle t1) {
                if (t0.start > t1.start)
                    return 1;
                return -1;
            }
        });

        for(int i=0 ; i<N-1 ; i++) {
            long beforeCircleEnd = circles[i].end;
            for(int j=i+1 ; j<N && circles[j].start<= beforeCircleEnd; j++) { // for문에 넣은 코드 효과 있음. (62->75)
                output++;
                if (output > 10E6) {
                    return -1; //break보다 return코드가 더 빠름 (75->87)
                }
            }
        }

        return output;
    }
}
