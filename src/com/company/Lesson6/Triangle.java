package com.company.Lesson6;

import java.util.*;

//https://codility.com/demo/results/trainingACN8DK-XSR/
//93%
public class Triangle {
    public static int solution(int[] A) {
        int output = 0;
        Arrays.sort(A);

        for(int i=0 ; i<A.length-2 ; i++) {
            if ((long)(A[i] + A[i+1]) > (long)A[i+2]) {
                output = 1;
                break;
            }
        }

        return output;
    }
}
