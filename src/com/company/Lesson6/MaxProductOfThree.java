package com.company.Lesson6;

import java.util.*;

//https://codility.com/demo/results/trainingKKP32Z-MKK/
//44%
//https://codility.com/demo/results/trainingVTXB3T-SBF/
//55%
//https://codility.com/demo/results/trainingT5NJ3H-MXG/
//44$
//https://codility.com/demo/results/training2JN9HE-KZA/
//66%
//https://codility.com/demo/results/trainingKWV9WC-24X/
//100%
//https://codility.com/demo/results/trainingHSWMYX-ME4/
//100%
public class MaxProductOfThree {
    public static int solution(int[] A) {
        int N = A.length;
        Arrays.sort(A);

        int ppp = A[N-3]*A[N-2]*A[N-1];
        int mmp = A[0]*A[1]*A[N-1];

        if (ppp > mmp) {
            return ppp;
        }
        return mmp;
    }
}
