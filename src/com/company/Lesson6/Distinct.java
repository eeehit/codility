package com.company.Lesson6;

//https://codility.com/demo/results/trainingUVT4CN-B4W/
//100%
public class Distinct {
    public static int solution(int[] A) {
        boolean[] matrix = new boolean[2000001];
        int output = 0;

        for(int i=0 ; i<A.length ; i++) {
            matrix[A[i]+1000000] = true;
        }
        for(int i=0 ; i<matrix.length ; i++) {
            if (matrix[i] == true) {
                output++;
            }
        }

        return output;
    }
}
