package com.company.Lesson7;

import java.util.*;

/**
 * Created by trinity on 2017-11-08.
 */
//100% 100% 100%
//https://codility.com/demo/results/trainingSGWN7Q-393/
public class StoneWall {
    public static int solution(int[] H) {
        int result = 0;
        Stack<Integer> stack = new Stack<Integer>();

        for(int h: H) {
            if (stack.isEmpty()) {
                stack.push(h);
            } else if (!stack.isEmpty() && h < stack.peek()) {
                while (!stack.isEmpty() && h < stack.peek()) {
                    stack.pop();
                    result++;
                }
                if ((!stack.isEmpty() && h > stack.peek()) || stack.isEmpty()) {
                    stack.push(h);
                }
            } else if (!stack.isEmpty() && h > stack.peek()) {
                stack.push(h);
            }
        }

        return result+stack.size();
    }
}
