package com.company.Lesson7;

import java.util.*;

/**
 * Created by trinity on 2017-11-09.
 */
//https://codility.com/demo/results/trainingSCZX99-JFD/
//100% 100% 100%
public class Nesting {
    public int solution(String S) {
        if (S.length() % 2 == 1) {
            return 0;
        }

        Stack<Character> stack = new Stack<Character>();

        for(char s:S.toCharArray()) {
            if (!stack.isEmpty() && stack.peek()== '(' && s == ')') {
                stack.pop();
            } else {
                stack.push(s);
            }
        }

        if (stack.isEmpty()) {
            return 1;
        }
        return 0;
    }
}
