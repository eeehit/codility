package com.company.Lesson7;

import java.util.*;

//https://codility.com/demo/results/trainingSXMWEY-VS9/
//50% 25% 37%
//https://codility.com/demo/results/trainingB59FRF-6C5/
//50% 50% 50%
//https://codility.com/demo/results/trainingER9BXR-WSU/
//75% 25% 50%
//https://codility.com/demo/results/trainingPBUHW6-HSG/
//100% 100% 100%
public class Fish {
    public int solution(int[] A, int[] B) {
        Stack<Integer> stack = new Stack<Integer>();

        int Q = 0;
        while(Q < A.length) {
            if (stack.isEmpty()) {
                stack.push(Q);
                Q++;
            } else {
                int P = stack.peek();
                if (B[P] == 1 && B[Q]==0) { //downstream과 upstream이 만날때
                    if (A[P] > A[Q]) { // downstream이 이겼을 때
                        Q++;
                    } else { // upstream이 이겼을 때
                        stack.pop();
                    }
                } else {
                    stack.push(Q);
                    Q++;
                }
            }
        }

        return stack.size();
    }
}
