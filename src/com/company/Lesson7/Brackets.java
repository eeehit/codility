package com.company.Lesson7;

import java.util.*;

/**
 * Created by trinity on 2017-11-08.
 */
//https://codility.com/demo/results/trainingBV4HJG-2X4/
//100% 80% 87%
//https://codility.com/demo/results/trainingJW27ZW-W3C/
//100% 100% 100%
public class Brackets {
    public int solution(String S){
        if (S.length() % 2 == 1) {
            return 0;
        }

        Stack<Character> stack = new Stack<Character>();

        for(char s:S.toCharArray()) {
            if (stack.isEmpty()) {
                stack.push(s);
            } else if (!stack.isEmpty() && stack.peek()== '(' && s == ')') {
                stack.pop();
            } else if (!stack.isEmpty() && stack.peek()== '{' && s == '}') {
                stack.pop();
            } else if (!stack.isEmpty() && stack.peek()== '[' && s == ']') {
                stack.pop();
            } else {
                stack.push(s);
            }
        }

        if (stack.isEmpty()) {
            return 1;
        }
        return 0;
    }
}
