package com.company.Lesson7;

import org.junit.Assert;
import org.junit.Test;

public class FishTest {
    @Test
    public void solution() throws Exception {
        Fish fish = new Fish();
        Assert.assertEquals(2, fish.solution(new int[] {4,3,2,1,5}, new int[] {0,1,0,0,0}));
    }
    @Test
    public void solution2() throws Exception {
        Fish fish = new Fish();
        Assert.assertEquals(5, fish.solution(new int[] {4,3,2,1,5}, new int[] {0,0,0,0,0}));
    }
    @Test
    public void solution3() throws Exception {
        Fish fish = new Fish();
        Assert.assertEquals(3, fish.solution(new int[] {9,7,6,2,4,5}, new int[] {1,1,1,1,0,0}));
    }
    @Test
    public void solution4() throws Exception {
        Fish fish = new Fish();
        Assert.assertEquals(2, fish.solution(new int[] {1,2,3,2,4,5}, new int[] {1,1,1,1,0,0}));
    }
    @Test
    public void extreme_small () throws Exception {
        Fish fish = new Fish();
        Assert.assertEquals(2, fish.solution(new int[] {0,1}, new int[] {1,1}));
    }
}
