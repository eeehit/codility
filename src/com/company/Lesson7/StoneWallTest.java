package com.company.Lesson7;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by trinity on 2017-11-08.
 */
public class StoneWallTest {
    @Test
    public void solution() throws Exception {
        Assert.assertEquals(7, StoneWall.solution(new int[]{8,8,5,7,9,8,7,4,8}));
    }

    @Test
    public void solution1() throws Exception {
        Assert.assertEquals(1, StoneWall.solution(new int[]{1}));
    }
}
