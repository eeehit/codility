package com.company.Lesson13;

//https://codility.com/demo/results/training5BPP4R-N8N/
//75% 0% 37%
//https://codility.com/demo/results/trainingRFGCHX-3WM/
//100% 100% 100%
public class Ladder {
    public static int[] solution(int[] A, int[] B) {
        int[] L = new int[A.length];

        int max = 3;
        for(int a:A) {
            if (a > max) {
                max = a;
            }
        }

        long[] f = new long[max+1];
        f[0] = 0;
        f[1] = 1;
        f[2] = 2;

        for(int i=3 ; i<max+1 ; i++) {
            f[i] = (f[i-1] + f[i-2]) % (int)Math.pow(2,30); //2^30을 넘지 않을 계획이기 때문에 진작에 modular 연산한다.
        }

        for(int i=0 ; i<L.length ; i++) {
            long tmp = (long) Math.pow(2,B[i]);
            L[i] = (int) (f[A[i]] % tmp);
        }
        return L;
    }
}
