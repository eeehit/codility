package com.company.Lesson5;

/**
 * Created by trinity on 2017-10-26.
 */
//https://codility.com/demo/results/trainingYC26U4-YX2/
//50%
//https://codility.com/demo/results/trainingZ2RBDT-MEN/
//62%
//https://codility.com/demo/results/trainingYFU492-XHQ/
//12%
//https://codility.com/demo/results/trainingABTGEX-8JU/
//100%
public class CountDiv {
    public static int solution(int A, int B, int K) {
        int output = 0;
        for(int i=A ; i<=B;) {
            if (i % K == 0) {
                output = (B-i)/K + 1;
                break;
            } else {
                i++;
            }
        }
        return output;
    }
}
