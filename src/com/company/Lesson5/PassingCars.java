package com.company.Lesson5;

//https://codility.com/demo/results/training4QEF8Y-5Q2/
//70%
//https://codility.com/demo/results/training285JU9-PZE/
//90%
//https://codility.com/demo/results/trainingAZNBCF-E6K/
//100%
public class PassingCars {
    public static int solution(int[] A) {
        long output = 0;
        long count1 = 0;
        for(int i=A.length-1 ; i>=0; i--) {
            if (A[i] == 1) {
                count1++;
            } else {
                output += count1;
            }
        }
        if (output > 1000000000) {
            return -1;
        }
        return (int)output;
    }
}
