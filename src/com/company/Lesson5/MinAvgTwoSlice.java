package com.company.Lesson5;

//https://codility.com/demo/results/training5WVPYX-JQ7/
//50%
//https://codility.com/demo/results/trainingDR4BC7-SFT/
//60%
//https://codility.com/demo/results/trainingWVB868-TDE/
//100%
public class MinAvgTwoSlice {
    public static int solution(int[] A) {
        int output = 0;
        double average = (A[0]+A[1])/2;

        for(int P=0 ; P<A.length ; P++){
            if(P+1 < A.length && P != 0) {
                double tmp = (double) (A[P] + A[P+1]) / 2;
                if (average > tmp) {
                    average = tmp;
                    output = P;
                }
            }
            if(P+2 < A.length) {
                double tmp = (double) (A[P] + A[P+1] + A[P+2]) / 3;
                if (average > tmp) {
                    average = tmp;
                    output = P;
                }
            }
        }

        return output;
    }
}
