package com.company.Lesson5;

//https://codility.com/demo/results/trainingGHXQNG-A55/
//100%
public class GenomicRangeQuery {
    public static int[] solution(String S, int[] P, int[] Q) {
        int N = S.length();
        int matrix[][] = new int[5][N+1];
        int output[] = new int[P.length];

        for(int i=0 ; i<N ; i++) {
            char c = S.charAt(i);
            if (c == 'A') {
                matrix[1][i+1] = matrix[1][i]+1;
            } else if (c == 'C') {
                matrix[2][i+1] = matrix[2][i]+1;
            } else if (c == 'G') {
                matrix[3][i+1] = matrix[3][i]+1;
            } else {
                matrix[4][i+1] = matrix[4][i]+1;
            }

            for(int j=1 ; j<5 ; j++) {
                if(matrix[j][i+1] == 0)
                    matrix[j][i+1] = matrix[j][i];
            }
        }

        for(int i=0 ; i<P.length ; i++) {
            int p = P[i];
            int q = Q[i]+1;

            for(int j=1 ; j<5 ; j++) {
                if (matrix[j][q] != matrix[j][p]) {
                    output[i] = j;
                    break;
                }
            }
        }

        return output;
    }
}
