package com.company.Lesson9;

//https://codility.com/demo/results/training244K3R-HUA/
//100% 100% 100%
public class MaxSliceSum {
    public int solution(int[] A) {
        if (A.length == 0) {
            return 0;
        }

        int slice = 0;
        int sum = A[0];

        for (int a:A) {
            slice = Math.max(a, slice+a);
            sum = Math.max(sum, slice);
        }

        return sum;
    }
}
