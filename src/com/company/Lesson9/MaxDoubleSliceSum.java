package com.company.Lesson9;

//https://codility.com/demo/results/trainingXTWYHX-HKY/
//33% 71% 53%
//https://codility.com/demo/results/trainingESD9PU-CWK/
//50% 71% 61%
//https://codility.com/demo/results/trainingJF3TQX-EBZ/
//83% 71% 76%
//https://codility.com/demo/results/training22WCQV-MRQ/
//66% 71% 69%
//https://codility.com/demo/results/trainingKQR5U8-U8Q/
//83% 85% 84%
//https://codility.com/demo/results/trainingKDRUMM-RF6/
//100% 100% 100%
public class MaxDoubleSliceSum {
    public int solution(int[] A) {
        int N = A.length;
        if (N <= 3) {
            return 0;
        }
        int maxSum = 0;
        int[] leftSum = new int[N];
        int[] rightSum = new int[N];

        for(int X=1 ; X<=N-3 ; X++) {
            leftSum[X] = Math.max(leftSum[X-1] + A[X], 0);
        }
        for(int Z=N-2 ; Z>=2 ; Z--) {
            rightSum[Z] = Math.max(rightSum[Z+1] + A[Z], 0);
        }
        for(int Y=1 ; Y<=N-2 ; Y++) {
            maxSum = Math.max(maxSum, leftSum[Y-1]+rightSum[Y+1]);
        }

        return maxSum;

    }
}
