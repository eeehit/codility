package com.company.Lesson9;

import org.junit.Assert;
import org.junit.Test;

public class MaxProfitTest {
    private MaxProfit maxProfit = new MaxProfit();
    @Test
    public void solution() throws Exception {
        Assert.assertEquals(356, maxProfit.solution(new int[]{23171, 21011, 21123, 21366, 21013, 21367}));
    }
    @Test
    public void max_profit_after_max_and_before_min() throws Exception {
        Assert.assertEquals(3, maxProfit.solution(new int[]{8, 9, 3, 6, 1, 2}));
    }
    @Test
    public void golden_max_slice() throws Exception {
        Assert.assertEquals(10, maxProfit.golden_max_slice(new int[]{5,-7,3,5,-2,4,-1}));
    }
}
