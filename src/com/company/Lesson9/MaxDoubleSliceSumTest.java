package com.company.Lesson9;

import org.junit.Assert;
import org.junit.Test;


public class MaxDoubleSliceSumTest {
    private MaxDoubleSliceSum maxDoubleSliceSum = new MaxDoubleSliceSum();
    @Test
    public void solution() throws Exception {
        Assert.assertEquals(17, maxDoubleSliceSum.solution(new int[]{3, 2, 6, -1, 4, 5, -1, 2}));
    }
    @Test
    public void extreme_triplet () throws Exception {
        Assert.assertEquals(0, maxDoubleSliceSum.solution(new int[]{5, 5, 5}));
    }

    @Test
    public void simple1 () throws Exception {
        Assert.assertEquals(17, maxDoubleSliceSum.solution(new int[]{5, 17, 0, 3}));
    }

    @Test
    public void simple2 () throws Exception {
        Assert.assertEquals(1, maxDoubleSliceSum.solution(new int[]{-2, -3, -4, 1, -5, -6, -7}));
    }
    @Test
    public void simple3 () throws Exception {
        Assert.assertEquals(1, maxDoubleSliceSum.solution(new int[]{5, 0, 1, 0, 5}));
    }

    @Test
    public void simple4 () throws Exception {
        Assert.assertEquals(10, maxDoubleSliceSum.solution(new int[]{0, 10, -5, -2, 0}));
    }

    @Test
    public void medium_range() throws Exception {
        int[] input = new int[2001];
        int x = -1000;
        for(int i=0 ; i<input.length ; i++) {
            input[i] = x;
            x++;
        }
        Assert.assertEquals(499500, maxDoubleSliceSum.solution(input));
    }

    @Test
    public void positive () throws Exception {
        Assert.assertEquals(26, maxDoubleSliceSum.solution(new int[]{6, 1, 5, 6, 4, 2, 9, 4}));
    }
}