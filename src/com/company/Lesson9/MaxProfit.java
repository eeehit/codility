package com.company.Lesson9;

//https://codility.com/demo/results/trainingRKM83S-RFY/
//100% 25% 66%
//https://codility.com/demo/results/trainingQZUBU6-A4V/
//100% 100% 100%
public class MaxProfit {
    public int solution(int[] A) {
        if (A.length <= 1) {
            return 0;
        }

        int buy = A[0];
        int margin = 0;

        for (int sell:A) {
            buy = Math.min(sell, buy);
            margin = Math.max(margin, sell-buy);
        }

        return margin;
    }

    //https://codility.com/media/train/7-MaxSlice.pdf
    public int golden_max_slice(int[] A) {
        int max_ending = 0;
        int max_slice = 0;

        for(int a:A) {
            max_ending = Math.max(0, max_ending + a);
            max_slice = Math.max(max_slice, max_ending);
        }
        return max_slice;
    }
}
