package com.company.Lesson2;

/**
 * Created by trinity on 2017-10-21.
 */
//https://codility.com/demo/results/training8XHD65-3TB/
//88%
//https://codility.com/demo/results/trainingWYX34S-A8S/
//100%
public class OddOccurrencesInArray {
    public static int solution(int[] A) {
        int result = 0;

        for(int num : A) {
            result = result ^ num;
        }

        return result;
    }
}
