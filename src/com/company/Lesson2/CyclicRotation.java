package com.company.Lesson2;

/**
 * Created by trinity on 2017-10-21.
 */
//https://codility.com/demo/results/trainingK36FQ7-TF2
//100%
public class CyclicRotation {
    public int[] solution(int[] A, int K) {
        int[] result = new int[A.length];
        for (int i=0 ; i<A.length ; i++) {
            result[(i+K) % A.length] = A[i];
        }
        return result;
    }
}
