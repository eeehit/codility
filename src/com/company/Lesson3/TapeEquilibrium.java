package com.company.Lesson3;

/**
 * Created by trinity on 2017-10-21.
 */
//https://codility.com/demo/results/trainingBPYYTA-Z68/
//41%
//https://codility.com/demo/results/trainingWB2XUM-T8G/
//58%
//https://codility.com/demo/results/trainingMRC7GE-BVQ/
//66%
//https://codility.com/demo/results/training8AGXQK-NJ2/
//75%
//https://codility.com/demo/results/trainingKXCMBA-P9Q/
//75%
//https://codility.com/demo/results/training7N496W-5KK/
//50%
//https://codility.com/demo/results/training9TS9JV-2UX/
//41%
//https://codility.com/demo/results/trainingFHVDEP-S8K/
//50%
//https://codility.com/demo/results/trainingATKMQ4-7YJ/
//100%
public class TapeEquilibrium {
    public static int solution(int[] A) {
        int totalA = 0;
        for(int num : A) {
            totalA += num;
        }
        int left = A[0];
        int minDifference = Math.abs(left - (totalA-left));

        for(int P=1 ; P<A.length-1 ; P++) {
            int currentLeft = left+A[P];
            int currentDifference = Math.abs(currentLeft - (totalA-currentLeft));
            if (minDifference > currentDifference) {
                minDifference = currentDifference;
            }
            left = currentLeft;
        }

        return minDifference;
    }
}
