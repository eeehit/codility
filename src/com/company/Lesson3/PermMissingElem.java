package com.company.Lesson3;

/**
 * Created by trinity on 2017-10-21.
 */
//https://codility.com/demo/results/training7C9WFS-C9V/
//100%
public class PermMissingElem {
    public int solution(int[] A) {
        int sumN = 0;
        int sumA = 0;

        for(int i=1 ; i<=A.length+1 ; i++) {
            sumN += i;
        }
        for(int i=0 ; i<A.length ; i++) {
            sumA += A[i];
        }
        return sumN-sumA;
    }
}
