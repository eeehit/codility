package com.company.Lesson3;

/**
 * Created by trinity on 2017-10-25.
 */
//https://codility.com/demo/results/trainingVND2WH-R5U/
//44%
//https://codility.com/demo/results/trainingNAUM7D-WNZ/
//100%
public class FrogJmp {
    public int solution(int X, int Y, int D) {
        return (int) Math.ceil( (Y-X) / (double)D );
    }
}
