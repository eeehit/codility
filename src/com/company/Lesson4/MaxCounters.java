package com.company.Lesson4;

import java.util.*;

/**
 * Created by trinity on 2017-10-25.
 */
//https://codility.com/demo/results/training768T3A-UZM/
//77%
//https://codility.com/demo/results/trainingZCY2VM-66P/
//100%
public class MaxCounters {
    public static int[] solution(int N, int[] A) {
        int matrix[] = new int[N];
        int max = 0;
        int min = 0;

        for(int K=0 ; K<A.length ; K++) {
            int X = A[K];
            int index = X-1;
            if ( X >=1 && X <= N) {
                if (matrix[index]<min) {
                    matrix[index] = min;
                }
                matrix[index]++;

                if (max < matrix[index]) {
                    max = matrix[index];
                }
            } else if (X == N+1) {
                min = max;
            }
        }

        for(int i=0 ; i<N ; i++) {
            if (matrix[i] < min) {
                matrix[i] = min;
            }
        }

        return matrix;
    }
}
