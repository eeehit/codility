package com.company.Lesson4;

/**
 * Created by trinity on 2017-10-25.
 */
//https://codility.com/demo/results/trainingCPGHH6-2R3/
//100%
public class PermCheck {
    public int solution(int[] A) {
        boolean[] matrix = new boolean[A.length+1];

        for(int i=0 ; i<A.length ; i++) {
            if (A[i] > A.length) {
                return 0;
            }
            matrix[A[i]] = true;
        }

        for(int i=1 ; i<matrix.length ; i++) {
            if (matrix[i] != true) {
                return 0;
            }
        }
        return 1;
    }
}
