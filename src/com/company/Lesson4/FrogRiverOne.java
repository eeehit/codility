package com.company.Lesson4;

import java.util.*;

/**
 * Created by trinity on 2017-10-25.
 */
//https://codility.com/demo/results/trainingGNB2K5-NNH/
//72%
//https://codility.com/demo/results/trainingCXTPZN-66G/
//90%
//https://codility.com/demo/results/trainingFJUFHX-AAX/
//100%
public class FrogRiverOne {
    public static int solution(int X, int[] A) {
        int[] river = new int[X+1];
        Arrays.fill(river, -1);
        int result = -1;

        for(int i=0 ; i<A.length ; i++) {
            if (river[A[i]] == -1) {
                river[A[i]] = i;
            }
        }

        for(int i=1 ; i<river.length ;i++) {
            if (river[i] == -1) {
                result = -1;
                break;
            }
            if (result < river[i]) {
                result = river[i];
            }
        }
        return result;
    }
}
