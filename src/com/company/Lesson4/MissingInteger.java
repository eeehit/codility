package com.company.Lesson4;

import java.util.*;

/**
 * Created by trinity on 2017-10-25.
 */
//https://codility.com/demo/results/trainingET2EB2-SWM/
//100%
public class MissingInteger {
    public static int solution(int[] A) {
        int[] matrix = new int[A.length+2];
        Arrays.fill(matrix, -1);
        int result = -1;

        for(int i=0 ; i<A.length ; i++) {
            if (A[i] > 0 && A[i] <= A.length) {
                if (matrix[A[i]] == -1) {
                    matrix[A[i]] = i;
                }
            }
        }

        for(int i=1 ; i<matrix.length ; i++) {
            if (matrix[i] == -1) {
                result = i;
                break;
            }
        }
        return result;
    }
}
