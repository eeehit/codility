package com.company.Lesson8;

//https://codility.com/demo/results/trainingTKQ7DH-WMV/
//100% 100% 100%
public class Dominator {
    public int solution(int[] A) {
        int N = A.length;

        int size = 0;
        int value = 0;
        for(int k=0 ; k<N ; k++) {
            if (size == 0) {
                size++;
                value = A[k];
            } else {
                if (value != A[k]) {
                    size--;
                } else {
                    size++;
                }
            }
        }

        int candidate = -1;
        if (size > 0) {
            candidate = value;
        }

        int leader = -1;
        int count = 0;
        for(int k=0 ; k<N ; k++) {
            if (A[k] == candidate) {
                count++;
            }
        }
        if (count > N/2) {
            leader = candidate;
        }

        for(int k=0 ; k<N ; k++) {
            if (A[k] == leader) {
                return k;
            }
        }

        return -1;
    }
}
