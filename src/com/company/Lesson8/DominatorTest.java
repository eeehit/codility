package com.company.Lesson8;

import org.junit.Assert;
import org.junit.Test;

public class DominatorTest {
    private Dominator dominator = new Dominator();

    @Test
    public void solution() throws Exception {
        Assert.assertEquals(0, dominator.solution(new int[] {3,4,3,2,3,-1,3,3}));
    }
}
