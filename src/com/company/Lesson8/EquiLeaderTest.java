package com.company.Lesson8;

import org.junit.Assert;
import org.junit.Test;

public class EquiLeaderTest {
    @Test
    public void solution() throws Exception {
        EquiLeader equiLeader = new EquiLeader();
        Assert.assertEquals(2, equiLeader.solution(new int[] {4,3,4,4,4,2}));
    }
    @Test
    public void simple() throws Exception {
        EquiLeader equiLeader = new EquiLeader();
        Assert.assertEquals(3, equiLeader.solution(new int[] {4, 4, 2, 5, 3, 4, 4, 4}));
    }

    @Test
    public void small_random() throws Exception {
        EquiLeader equiLeader = new EquiLeader();
        Assert.assertEquals(3, equiLeader.solution(new int[] {1, 2, 1, 1, 2, 1}));
    }
}
