package com.company.Lesson8;

import java.util.*;

//https://codility.com/demo/results/trainingXX4QTN-ECE/
//80% 0% 44%
//https://codility.com/demo/results/trainingAS5M82-GTQ/
//100% 0% 55%
//https://codility.com/demo/results/trainingR7PVHS-VSY/
//60% 25% 44%
//https://codility.com/demo/results/trainingGJFT63-EYD/
//100% 100% 100%
public class EquiLeader {
    public int solution(int[] A){
        int result = 0;
        int leaders = 0;
        int count = 0;
        int N = A.length;


        int B[] = A.clone();
        Arrays.sort(B);
        int candidate = B[(int)Math.ceil(N/2)];
        for(int k=0 ; k<N ; k++) {
            if (candidate == B[k]) {
                count++;
            }
        }
        if (count > N/2) {
            leaders = candidate;
        } else {
            return 0;
        }

        int first = 0;
        for (int S=0 ; S<N-1; S++) {
            if (A[S] == leaders) {
                first++;
            }
            if (count-first > (N-1-S)/2 && first > (S+1)/2) {
                result++;
            }
        }
        return result;
    }
}
